# Maintainer <-> Raute Insights integration proposal

The aim of this document is to provide a proposal of integration between Raute Insights -portal and Maintainer (M-Files CMMS) system.

## Integration lifecycle
The two systems are able to talk to each other using ``application/json`` encoded HTTP-messages. The messages are send from one server and received by another. Depending on the message type additional actions are performed.

As the objects are updated in Maintainer it should trigger an update notification that is sent to Insights server.  This triggers a request to fetch new data from Maintainer to Insights by accessing Maintainer through MFWS-API. The fetched data is then saved either internally to Insights. The object type defines whether Maintainer should proceed with triggering the update procedure.

The following list illustrates the lifecycle of an update event.

1. User updates a field in an Maintainer object.
2. Maintainer validates this update and if it matches the validation (i.e. event matches one of predefined object types) proceeds with sending a HTTP-POST request to Insights server. This request contains information on what objects have been created or modified.
3. Insights server receives the HTTP-POST request and sends an appropriate response to Maintainer server.
4. Insights server queries the Maintainer server for objects that have been updated. The updated objects are listed in the notification request (see notification structure).
5. The Maintainer server responds with updated objects and related metadata.
6. Insights server receives these updates and processes them accordingly.


## Request / Response structure

### Initial request from Maintainer to Insights

The following is adapted from MFWS documentation. The keys should correspond to the updated object.

**Request:**

```
POST /notifyUpdate HTTP/1.1
Host: cmms.insights.raute.com
Content-Type: application/json
Cache-Control: no-cache

{
  "type": 1234,
  "objectid": 1234,
  "version": 1234,
  "file": 1234,
  "id": 1234,
  "path": "/"
}
```

**Response(body):**

On succesful request

``{"status": "success"}``

On failed request

``{"status": "fail"}``


### Requesting updated objects from Maintainer to Insights

Requesting objects from Maintainer server to Insights is executed using MFWS API. The posted object is used as basis for compiling the query.

This is a real query from https://mfiles.raute.com

**Request**
```
GET /REST/objects/117/162/2/properties.aspx
Host: https://mfiles.raute.com
```

**Response**
```
[
  {
    "TypedValue": {
      "Value": "XY cylinder clearances 3 Left",
      "HasValue": true,
      "DisplayValue": "XY cylinder clearances 3 Left",
      "SerializedValue": "XY%20cylinder%20clearances%203%20Left",
      "DataType": 1,
      "SortingKey": "",
      "HasAutomaticPermission": false
    },
    "PropertyDef": 0,
    "ContentType": 0
  },
  {
    "TypedValue": {
      "Value": "2016-04-26T06:38:12Z",
      "HasValue": true,
      "DisplayValue": "4/26/2016 6:38 AM",
      "SerializedValue": "2016-04-26T06%3A38%3A12",
      "DataType": 7,
      "SortingKey": "635972494920000000",
      "HasAutomaticPermission": false
    },
    "PropertyDef": 21,
    "ContentType": 0
  }
  ...The list continues...
]
```
